package com.enoxs.filebrowser.infra.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.enoxs.filebrowser.infra.broadcast.listener.SDCardReceiverListener;

import java.util.logging.Logger;

public class SDCardReceiver extends BroadcastReceiver {
    private Logger log = Logger.getLogger(this.getClass().getName());

    private String SD_IN = "android.intent.action.MEDIA_MOUNTED";
    private String SD_OUT = "android.intent.action.MEDIA_UNMOUNTED";

    private SDCardReceiverListener listener;

    public SDCardReceiver(SDCardReceiverListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        log.info("Action => " + intent.getAction());
        if(SD_IN.equals(intent.getAction())){
            listener.showNavSdCardView();
            log.info("SD Card => Input");
        }else if(SD_OUT.equals(intent.getAction())){
            listener.hideNavSdCardView();
            log.info("SD Card => Output");
        }
    }

}
