package com.enoxs.filebrowser.infra.broadcast.listener;

public interface SDCardReceiverListener {
    void showNavSdCardView();
    void hideNavSdCardView();
}
