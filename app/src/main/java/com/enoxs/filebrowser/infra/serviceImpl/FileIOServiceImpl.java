package com.enoxs.filebrowser.infra.serviceImpl;

import android.os.StatFs;
import android.text.format.Formatter;

import com.enoxs.filebrowser.domain.service.FileIOService;
import com.enoxs.filebrowser.infra.utill.FileUtil;

import java.io.File;
import java.util.List;

public class FileIOServiceImpl implements FileIOService {

    private FileUtil fileUtil = new FileUtil();


    @Override
    public boolean isExist(String path){
        return fileUtil.isExist(path);
    }

    @Override
    public boolean isMounted(String path){
        boolean isMounted = false;
        File file = new File(path);
        StatFs stat = new StatFs(file.getPath());
        long totalBlocks = stat.getBlockCountLong();

        if(totalBlocks > 0){
            isMounted = true;
        }

        return isMounted;
    }

    @Override
    public String load(String path){
        return fileUtil.load(path);
    }

    @Override
    public List<String[]> listDirInfo(String path){
        return fileUtil.listDirInfo(path);
    }

    @Override
    public boolean createFile(String path, String name) {
        return fileUtil.make(path,name);
    }

    @Override
    public boolean createFolder(String path) {
        return fileUtil.make(path);
    }


}
