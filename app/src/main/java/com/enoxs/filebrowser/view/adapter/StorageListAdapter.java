package com.enoxs.filebrowser.view.adapter;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.filebrowser.R;
import com.enoxs.filebrowser.view.listener.StorageFragmentListener;

import java.util.List;
import java.util.logging.Logger;

public class StorageListAdapter extends RecyclerView.Adapter<StorageListAdapter.ViewHolder> implements
        View.OnClickListener{

    private Logger log = Logger.getLogger(this.getClass().getName());

    private OnRecyclerViewItemClickListener mOnItemClickListener = null;

    private StorageFragmentListener listener = null;


    private List<Integer> lstImage;
    private List<String> lstTitle;
    private List<String> lstSubtitle;

    int lastX, lastY;

    // Enoxs Adjust : 檔案列表，按鈕點擊事件
    // Enoxs Adjust : 檔案列表，按鈕點擊效果
    // Enoxs Impl : 導航列與檔案列表，瀏覽功能

    public StorageListAdapter( StorageFragmentListener listener , List<Integer> lstImage, List<String> lstTitle , List<String> lstSubtitle) {
        this.lstImage = lstImage;
        this.lstTitle = lstTitle;
        this.lstSubtitle = lstSubtitle;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.storage_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        view.setOnClickListener(this);

        return viewHolder;//連接布局，新增一個view給viewholder綁定元件
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.icon.setImageResource(lstImage.get(position));
        holder.title.setText(lstTitle.get(position));
        holder.subtitle.setText(lstSubtitle.get(position));

        holder.itemView.setTag(position); // 點擊事件，取值的方法

        if(lstImage.get(position) == R.drawable.icon_previous){
            holder.subtitle.setVisibility(View.GONE);
            holder.layFileMenu.setVisibility(View.INVISIBLE);
            holder.layRow.setGravity(Gravity.CENTER);
        }else{
            holder.subtitle.setVisibility(View.VISIBLE);
            holder.layFileMenu.setVisibility(View.VISIBLE);
            holder.layRow.setGravity(Gravity.NO_GRAVITY);
        }


        holder.layFileMenu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int ea = event.getAction();
                switch (ea) {
                    case MotionEvent.ACTION_DOWN:   //按下
                        lastX = (int) event.getRawX();
                        lastY = (int) event.getRawY();
                        break;
                }
                return false;
            }
        });


        holder.layFileMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickFileMenuAction(lastX , lastY);
            }
        });

//        holder.layFileItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                log.info("click().");
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return lstTitle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;
        private TextView title;
        private TextView subtitle;
        private ImageView btnFileAction;
        private LinearLayout layFileItem , layFileMenu , layRow;

        public ViewHolder(View Holder) {
            super(Holder);
            //取得從onCreateViewHolder的view，此ViewHolder綁定主布局元件
            icon = Holder.findViewById(R.id.icon);
            title = Holder.findViewById(R.id.title);
            subtitle = Holder.findViewById(R.id.subtitle);
            btnFileAction = Holder.findViewById(R.id.btn_file_action);
            layFileItem = Holder.findViewById(R.id.lay_file_item);
            layFileMenu = Holder.findViewById(R.id.lay_file_menu);
            layRow = Holder.findViewById(R.id.lay_row);

            icon.getLayoutParams().width=150;
            icon.getLayoutParams().height=150;
        }
    }


    /**
     * RecyclerView.onClickEvent
     */

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, Integer index, int x , int y);
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(v, Integer.parseInt(v.getTag()+"") , lastX , lastY);
        }
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
