package com.enoxs.filebrowser.view.fragment.sdcard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.enoxs.filebrowser.databinding.FragmentSdcardBinding;

public class SDcardFragment extends Fragment {

    private SDCardViewModel storageViewModel;
    private FragmentSdcardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        storageViewModel =
                new ViewModelProvider(this).get(SDCardViewModel.class);

        binding = FragmentSdcardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.txtSdcard;
        storageViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}