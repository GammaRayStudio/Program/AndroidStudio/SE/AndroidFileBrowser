package com.enoxs.filebrowser.view.listener;

public interface StorageFragmentListener {

    void clickFileMenuAction(int x , int y);

    void createFile(String fileName);

    void createFolder(String folderName);
}
