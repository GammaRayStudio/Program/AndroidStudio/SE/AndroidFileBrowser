package com.enoxs.filebrowser.view;

import android.Manifest;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.enoxs.filebrowser.R;
import com.enoxs.filebrowser.domain.service.FileIOService;
import com.enoxs.filebrowser.infra.broadcast.SDCardReceiver;
import com.enoxs.filebrowser.infra.broadcast.listener.SDCardReceiverListener;
import com.enoxs.filebrowser.infra.serviceImpl.FileIOServiceImpl;
import com.enoxs.filebrowser.infra.utill.FileUtil;
import com.enoxs.filebrowser.view.fragment.storage.StorageFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.enoxs.filebrowser.databinding.ActivityMainBinding;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity implements SDCardReceiverListener {
    private Logger log = Logger.getLogger("MainActivity");

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;


    // FloatingActionButton
    FloatingActionButton fabAdd, fabAddFile, fabAddFolder;


    // MenuView
    private MenuItem btnSearch, btnLayout;
    private MenuItem actionSelect, actionSupport, actionAbout;

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private NavController navController;

    private boolean isFabState = false;
    private boolean isFileAccess = false;

    // 廣播接收器
    private SDCardReceiver sdCardReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);

        drawerLayout = binding.drawerLayout;
        navigationView = binding.navView;


        fabAdd = binding.appBarMain.fabAdd;
        fabAddFile = binding.appBarMain.fabAddFile;
        fabAddFolder = binding.appBarMain.fabAddFolder;

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFabState = !isFabState;
                toggleFabMenu(isFabState);

//                showAddItemDialog();
                // Enoxs Fix : /storage/MicroSD 為空，發生崩潰
//                List<String[]> lst = fileUtil.listDirInfo("/storage/MicroSD");
//                for (String[] msg : lst) {
//                    for (int i = 0; i < msg.length; i++) {
//                        log.info("info => " + msg[i]);
//
//                    }
//                }

                if(!isFileAccess){
                    checkFileAccess();
                }

            }
        });


        fabAddFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddFileDialog();
            }
        });

        fabAddFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddFolderDialog();
            }
        });


        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_storage, R.id.nav_micro_sd,
                R.id.nav_cloud, R.id.nav_cloud_add, R.id.nav_support,
                R.id.nav_about)
                .setOpenableLayout(drawerLayout)
                .build();


        navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



        checkFileAccess();

        if(isFileAccess){
            runFileAction();
        }



//        initNavSdCardView();
//        registerSDCardReceiver();
    }


    private void checkFileAccess(){
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.WRITE_EXTERNAL_STORAGE};

        int readState = ContextCompat.checkSelfPermission(MainActivity.this, permissions[0]);
        int writeState = ContextCompat.checkSelfPermission(MainActivity.this, permissions[1]);

        if (readState == PackageManager.PERMISSION_GRANTED && writeState == PackageManager.PERMISSION_GRANTED) {
            log.info("已獲取權限");
            isFileAccess = true;
        }else{
            log.info("未獲取權限");
            isFileAccess = false;
        }

        if(!isFileAccess){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 100);
            }
        }
    }


    private void runFileAction(){
        FileUtil fileUtil = new FileUtil();

        File exFile = Environment.getExternalStorageDirectory();
        String exPath = exFile.getPath();

        log.info("exPath => " + exPath);

        List<String []> lstFile = fileUtil.listDirInfo(exPath + "/FileBrowser");

        for (int i = 0; i < lstFile.get(0).length; i++) {
            log.info("folder => " + lstFile.get(0)[i]);
        }
        for (int i = 0; i < lstFile.get(1).length; i++) {
            log.info("file => " + lstFile.get(1)[i]);
        }

        log.info("create file.");

        String filePath = exPath + "/FileBrowser/Sample";
        String fileName = "template-text.md";
        String fileData = "Android\n======";
//        String fullPath = exPath + filePath;
//
        fileUtil.save(fileData , filePath , fileName);

    }


    // Enoxs FIX : Android 8.0 版本，虛擬機發生崩潰
    private void initNavSdCardView() {
        FileIOService service = new FileIOServiceImpl();
        toggleNavSdCardView(service.isMounted("/storage/MicroSD"));
    }

    int lastX = 0;
    int lastY = 0;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuCompat.setGroupDividerEnabled(menu, true);


        log.info("size() => " + menu.size());

        // Enoxs Impl : 偵測，目前的 Fragment
        String fragmentName = navController.getCurrentDestination().getLabel().toString();
        log.info("fragment name => " + fragmentName);

  /*      btnSearch = menu.findItem(R.id.btn_search);
        btnSearch.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log.info("Button Event => Search.");
                return false;
            }
        });


*/

        btnLayout = menu.findItem(R.id.btn_layout);
        btnLayout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log.info("Button Event => Refresh.");
                int[] location = new int[2];
                View view = findViewById(R.id.btn_layout);
                view.getLocationInWindow(location);
                int x = location[0]; // view距离window 左边的距离（即x轴方向）
                int y = location[1]; // view距离window 顶边的距离（即y轴方向）

                log.info("x => " + x);
                log.info("y => " + y);


                showLayoutSettingDialog(x , y );

                return false;
            }
        });

        actionSelect = menu.findItem(R.id.action_select);
        actionSelect.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log.info("Action Event => Select.");
                return false;
            }
        });


        actionSupport = menu.findItem(R.id.action_support);
        actionSupport.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log.info("Action Event => Support.");
                return false;
            }
        });

        actionAbout = menu.findItem(R.id.action_about);
        actionAbout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log.info("Action Event => About.");
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public void registerSDCardReceiver() {
        IntentFilter intentFilter = new IntentFilter();// 過濾器
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        sdCardReceiver = new SDCardReceiver(this);
        registerReceiver(sdCardReceiver, intentFilter);// 註冊廣播接收器
    }

    public void unRegisterReceiver() {
        if (sdCardReceiver != null) {
            unregisterReceiver(sdCardReceiver);// 註銷廣播接收器
        }
    }

    @Override
    public void showNavSdCardView() {
        toggleNavSdCardView(true);
    }


    @Override
    public void hideNavSdCardView() {
        toggleNavSdCardView(false);
    }

    // Enoxs Adjust: 簡化寫法 (findViewById)
    private void toggleNavSdCardView(boolean toggleState) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem group = menu.getItem(i);
            if (group.getItemId() == R.id.nav_local) {
                Menu subMenu = group.getSubMenu();
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem item = subMenu.getItem(j);
                    if (item.getItemId() == R.id.nav_micro_sd) {
                        item.setVisible(toggleState);
                    }
                }
            }
        }
    }

    private void toggleFabMenu(boolean toggleState) {
        if(toggleState){
            Animation menuAnim = new RotateAnimation(360, 0 ,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            menuAnim.setDuration(500);
            menuAnim.setStartOffset(50);

            Animation showAnim01 = new AlphaAnimation(0.0f, 1.0f);
            showAnim01.setDuration(500);
            showAnim01.setStartOffset(150);

            Animation showAnim02 = new AlphaAnimation(0.0f, 1.0f);
            showAnim02.setDuration(500);
            showAnim02.setStartOffset(300);

            fabAdd.startAnimation(menuAnim);
            fabAddFolder.startAnimation(showAnim01);
            fabAddFolder.setVisibility(View.VISIBLE);
            fabAddFile.startAnimation(showAnim02);
            fabAddFile.setVisibility(View.VISIBLE);
        }else{
            Animation menuAnim = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            menuAnim.setDuration(500);
            menuAnim.setStartOffset(50);

            Animation showAnim01 = new AlphaAnimation(1.0f, 0.0f);
            showAnim01.setDuration(500);
            showAnim01.setStartOffset(150);

            Animation showAnim02 = new AlphaAnimation(1.0f, 0.0f);
            showAnim02.setDuration(500);
            showAnim02.setStartOffset(350);

            fabAdd.startAnimation(menuAnim);
            fabAddFile.startAnimation(showAnim01);
            fabAddFile.setVisibility(View.GONE);
            fabAddFolder.startAnimation(showAnim02);
            fabAddFolder.setVisibility(View.GONE);
        }
    }


    public void showAddFileDialog() {//自訂義布局
        LayoutInflater adbInflater = LayoutInflater.from(this);
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        View alertView = adbInflater.inflate(R.layout.dialog_add_file_item, null);

        TextView title = (TextView) alertView.findViewById(R.id.dialog_title);
        EditText edtName =  (EditText) alertView.findViewById(R.id.edt_name);
        TextView extension = (TextView) alertView.findViewById(R.id.txt_ext);

        Button btnYes = (Button) alertView.findViewById(R.id.btnYes);
        Button btnNo = (Button) alertView.findViewById(R.id.btnNo);

        title.setText("新增檔案");
        btnYes.setText("建立檔案");
        extension.setVisibility(View.VISIBLE);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString() + ".md";
                log.info("name => " + name);

                Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
                if(navHostFragment != null){
                    StorageFragment fragment = (StorageFragment) navHostFragment.getChildFragmentManager().getFragments().get(0);
                    fragment.createFile(name);
                }
                dialog.cancel();
            }
        });

        btnYes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        btnYes.setTextColor(getResources().getColor(R.color.white));
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnYes.setTextColor(getResources().getColor(R.color.black));
                        break;

                }

                return false;
            }
        });


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        btnNo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        btnNo.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnNo.setTextColor(getResources().getColor(R.color.white));
                        break;

                }

                return false;
            }
        });
        dialog.setView(alertView);
        dialog.setCancelable(true);
        dialog.show();
    }

    public void showAddFolderDialog() {//自訂義布局
        LayoutInflater adbInflater = LayoutInflater.from(this);
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        View alertView = adbInflater.inflate(R.layout.dialog_add_file_item, null);
        TextView title = (TextView) alertView.findViewById(R.id.dialog_title);
        EditText edtName =  (EditText) alertView.findViewById(R.id.edt_name);
        TextView extension = (TextView) alertView.findViewById(R.id.txt_ext);
        Button btnYes = (Button) alertView.findViewById(R.id.btnYes);
        Button btnNo = (Button) alertView.findViewById(R.id.btnNo);

        title.setText("新增資料夾");
        btnYes.setText("建立資料夾");
        extension.setVisibility(View.GONE);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
                String name = edtName.getText().toString();

                if(navHostFragment != null){
                    StorageFragment fragment = (StorageFragment) navHostFragment.getChildFragmentManager().getFragments().get(0);
                    fragment.createFolder(name);
                }
                dialog.cancel();
            }
        });

        btnYes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        btnYes.setTextColor(getResources().getColor(R.color.white));
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnYes.setTextColor(getResources().getColor(R.color.black));
                        break;

                }

                return false;
            }
        });


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        btnNo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        btnNo.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case MotionEvent.ACTION_DOWN:
                        btnNo.setTextColor(getResources().getColor(R.color.white));
                        break;

                }

                return false;
            }
        });
        dialog.setView(alertView);
        dialog.setCancelable(true);
        dialog.show();
    }


    private void showLayoutSettingDialog(int x, int y) {//
        LayoutInflater adbInflater = LayoutInflater.from(this);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View checkboxLayout = adbInflater.inflate(R.layout.dialog_list_action, null);

        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        String[] folderMenu = {"選取", "移動到", "複製到", "重新命名", "刪除資料夾"};
        String[] fileMenu = {"選取", "移動到", "複製到", "重新命名", "刪除檔案", "釘選", "分享"};

        for (int i = 0; i < folderMenu.length; i++) {
            arrayAdapter.add(folderMenu[i]);
        }

        ListView first_list = (ListView) checkboxLayout.findViewById(R.id.list_more);
        first_list.setAdapter(arrayAdapter);
        first_list.setOnItemClickListener(new ListView.OnItemClickListener() {
            //new OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                log.info("position => " + position);
                alertDialog.cancel();
            }
        });

        alertDialog.setView(checkboxLayout);
        alertDialog.setCancelable(true);
        alertDialog.show();

        // 設定 Dialog 顯示的位置

        Window dialogWindow = alertDialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.x = x; // 新位置X坐標
        lp.y = y; // 新位置Y坐標
        lp.alpha = 1.0f;
        lp.dimAmount = 0.0f;

        WindowManager m = this.getWindowManager();

        Display d = m.getDefaultDisplay(); // 獲取屏幕寬、高用
        lp.width = (int) (d.getWidth() * 0.45); // 寬度設置為屏幕的0.65

        dialogWindow.setGravity(Gravity.LEFT | Gravity.TOP);
        dialogWindow.setAttributes(lp);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        log.info("code => " + requestCode);
        log.info("permission.length => " + permissions.length);

        boolean isRead = false;
        boolean isWrite = false;

        for (int i = 0; i < permissions.length; i++) {
            log.info(permissions[i]);
            if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permissions[i])) {
                log.info("READ_EXTERNAL_STORAGE");
                // 0 - 取得 , -1 - 未取得
                if (grantResults[i] == 0) {
                    log.info("讀取 權限已取得");
                    isRead = true;
                    isFileAccess = true;
                } else if (grantResults[i] == -1) {
                    log.info("讀取 權限未取得");
                    isRead = false;
                }
            }
            if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permissions[i])) {
                log.info("WRITE_EXTERNAL_STORAGE");
                // 0 - 取得 , -1 - 未取得
                if (grantResults[i] == 0) {
                    log.info("寫入 權限已取得");
                    isWrite = true;
                } else if (grantResults[i] == -1) {
                    log.info("寫入 權限未取得");
                    isWrite = false;
                }
            }
        }
        if(isRead == true && isWrite == true){
            isFileAccess = true;
        }else{
            isFileAccess = false;
        }

    }

}