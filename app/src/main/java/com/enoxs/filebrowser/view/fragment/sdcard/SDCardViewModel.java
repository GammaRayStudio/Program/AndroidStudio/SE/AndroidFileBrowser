package com.enoxs.filebrowser.view.fragment.sdcard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SDCardViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SDCardViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is sdcard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}