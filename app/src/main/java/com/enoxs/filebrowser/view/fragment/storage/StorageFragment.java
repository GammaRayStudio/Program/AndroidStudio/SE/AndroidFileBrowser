package com.enoxs.filebrowser.view.fragment.storage;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enoxs.filebrowser.R;
import com.enoxs.filebrowser.databinding.FragmentStroageBinding;
import com.enoxs.filebrowser.domain.service.FileIOService;
import com.enoxs.filebrowser.infra.serviceImpl.FileIOServiceImpl;
import com.enoxs.filebrowser.view.adapter.StorageListAdapter;
import com.enoxs.filebrowser.view.listener.StorageFragmentListener;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class StorageFragment extends Fragment implements StorageFragmentListener {
    private Logger log = Logger.getLogger(this.getClass().getName());

    private StorageViewModel storageViewModel;
    private FragmentStroageBinding binding;
    private LinearLayout layRouter, layNoData;

    private List<String> lstTitle = new LinkedList<>();
    private List<String> lstSubtitle = new LinkedList<>();
    private List<Integer> lstImage = new LinkedList<>();
    private RecyclerView recyclerView;
    private StorageListAdapter recyclerViewAdapter;

    private FileIOService fileIOService = new FileIOServiceImpl();


    private Integer previousId = R.drawable.icon_previous;
    private Integer folderId = R.drawable.icon_folder;
    private Integer fileImgId = R.drawable.icon_file;

    private String internalStorage = Environment.getExternalStorageDirectory().getPath(); // 內部儲存空間

    // Enoxs Impl : 保存 上一次開啟的路徑 (SharedPreferences)
    private String storagePath = Environment.getExternalStorageDirectory().getPath() + File.separator + "FileBrowser"; // 當前路徑


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        storageViewModel =
                new ViewModelProvider(this).get(StorageViewModel.class);

        binding = FragmentStroageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        recyclerView = binding.listStorage;

        layRouter = binding.layRouter;
        layNoData = binding.layNoData;

        loadNavPath();

        initStorageList();
        loadStroageList();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    // Enoxs Adjust : 檔案動作列表，功能動作清單
    // Enoxs Fix : Dialog 顯示的位置
    private void showFileActionDialog(int x, int y) {//
        LayoutInflater adbInflater = LayoutInflater.from(getContext());
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        View checkboxLayout = adbInflater.inflate(R.layout.dialog_list_action, null);

        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        String[] folderMenu = {"選取", "移動到", "複製到", "重新命名", "刪除資料夾"};
        String[] fileMenu = {"選取", "移動到", "複製到", "重新命名", "刪除檔案", "釘選", "分享"};

        for (int i = 0; i < folderMenu.length; i++) {
            arrayAdapter.add(folderMenu[i]);
        }

        ListView first_list = (ListView) checkboxLayout.findViewById(R.id.list_more);
        first_list.setAdapter(arrayAdapter);
        first_list.setOnItemClickListener(new ListView.OnItemClickListener() {
            //new OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                log.info("position => " + position);
                alertDialog.cancel();
            }
        });

        alertDialog.setView(checkboxLayout);
        alertDialog.setCancelable(true);
        alertDialog.show();

        // 設定 Dialog 顯示的位置

        Window dialogWindow = alertDialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.x = x; // 新位置X坐標
        lp.y = y; // 新位置Y坐標
        lp.alpha = 1.0f;
        lp.dimAmount = 0.0f;

        WindowManager m = getActivity().getWindowManager();

        Display d = m.getDefaultDisplay(); // 獲取屏幕寬、高用
        lp.width = (int) (d.getWidth() * 0.45); // 寬度設置為屏幕的0.65

        dialogWindow.setGravity(Gravity.LEFT | Gravity.TOP);
        dialogWindow.setAttributes(lp);
    }


    @Override
    public void clickFileMenuAction(int x, int y) {
        showFileActionDialog(x, y);
    }


    // Enoxs Impl : 路由功能

    /**
     * 載入導航列
     */
    private void loadNavPath() {
        boolean isExistPath = false;

        if (fileIOService.isExist(storagePath)) {
            isExistPath = true;
        }

        if (storagePath.startsWith(internalStorage) && isExistPath) {
            log.info("nav path => " + storagePath);
            String navPath = storagePath.replace(internalStorage, "內部儲存空間");
            String[] lstPath = navPath.split(File.separator);

            layRouter.removeAllViews();

            for (int i = 0; i < lstPath.length; i++) {
                TextView txt = new TextView(getContext());
                txt.setTag(i);
                txt.setText(lstPath[i]);
                txt.setTextSize(15);

                if (i == lstPath.length - 1) {
                    txt.setTextColor(Color.parseColor("#347BF2"));
                }

                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Enoxs Impl : 導航列 切換路徑
                        int pathLen = Integer.parseInt(v.getTag() + "") + 1;
                        StringBuffer newPath = new StringBuffer(32);

                        newPath.append(internalStorage);
                        for (int i = 1; i < pathLen; i++) {
                            if (i == 1){
                                newPath.append(File.separator);
                            }
                            newPath.append(lstPath[i]);
                            if (i < pathLen - 1) {
                                newPath.append(File.separator);
                            }

                        }

                        if(!storagePath.equals(newPath.toString())){
                            storagePath = newPath.toString();
                            log.info(storagePath);
                            loadNavPath();
                            loadStroageList();
                        }

                    }
                });

                layRouter.addView(txt);

                if (i < lstPath.length - 1) {
                    ImageView arrow = new ImageView(getContext());
                    arrow.setImageResource(R.drawable.icon_arrow_right);
                    layRouter.addView(arrow);
                }
            }

        }
    }


    private void initStorageList(){
        /**
         * Recycler View : 列表當前目錄的資訊
         */

        recyclerViewAdapter = new StorageListAdapter(this, lstImage, lstTitle, lstSubtitle);//將資料進行處理


        recyclerView.getItemAnimator().setMoveDuration(200);

        // 表格佈局
        GridLayoutManager gridManager = new GridLayoutManager(getContext(), 2);
        LinearLayoutManager lineManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(lineManager);
//        recyclerView.setLayoutManager(gridManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.setOnItemClickListener(new StorageListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, Integer index, int x, int y) {
                log.info("onItemClick().");
                log.info("index => " + index);

                switch (lstImage.get(index)) {
                    case R.drawable.icon_previous:
                        int last = storagePath.lastIndexOf(File.separator);
                        storagePath = storagePath.substring(0, last);
                        loadNavPath();
                        loadStroageList();
                        break;
                    case R.drawable.icon_folder:
                        storagePath += File.separator + lstTitle.get(index);
                        loadNavPath();
                        loadStroageList();
                        break;
                    case R.drawable.icon_file:
                        Bundle bundle = new Bundle();
                        bundle.putString("storagePath", storagePath);
                        bundle.putString("fileName", lstTitle.get(index));
                        NavHostFragment.findNavController(StorageFragment.this)
                                .navigate(R.id.to_file_fragment, bundle);
                        break;


                }

            }
        });

    }
    /**
     * 載入檔案列表
     */
    private void loadStroageList() {
        List<String[]> lstFile = fileIOService.listDirInfo(storagePath);

        lstImage.clear();
        lstTitle.clear();
        lstSubtitle.clear();

        log.info("storagePath => " + storagePath);
        if (!internalStorage.equals(storagePath)) {
            lstImage.add(previousId);
            lstTitle.add("返回上一頁");
            lstSubtitle.add("");
        }

        if (lstFile.size() > 0) {
            int fileAmount = lstFile.get(0).length + lstFile.get(1).length;

            if (fileAmount > 0) {
                // 資料夾
                for (int i = 0; i < lstFile.get(0).length; i++) {
                    log.info("folder => " + lstFile.get(0)[i]);
                    lstImage.add(folderId);
                    lstTitle.add(lstFile.get(0)[i]);
                    lstSubtitle.add("2021-11-30");
                }

                // 檔案
                for (int i = 0; i < lstFile.get(1).length; i++) {
                    log.info("file => " + lstFile.get(1)[i]);
                    lstImage.add(fileImgId);
                    lstTitle.add(lstFile.get(1)[i]);
                    lstSubtitle.add("2021-11-30");
                }

                ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                recyclerView.setLayoutParams(params);
                layNoData.setVisibility(View.GONE);
            } else {
                ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                recyclerView.setLayoutParams(params);
                layNoData.setVisibility(View.VISIBLE);
            }
            recyclerViewAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void createFile(String fileName){
        fileIOService.createFile(storagePath , fileName);
        loadStroageList();
    }

    @Override
    public void createFolder(String folderName){
        String path = storagePath + File.separator + folderName;
        fileIOService.createFolder(path);
        loadStroageList();
    }

}