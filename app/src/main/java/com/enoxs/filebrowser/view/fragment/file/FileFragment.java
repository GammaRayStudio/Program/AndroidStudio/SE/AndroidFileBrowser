package com.enoxs.filebrowser.view.fragment.file;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.enoxs.filebrowser.databinding.FragmentFileBinding;
import com.enoxs.filebrowser.domain.service.FileIOService;
import com.enoxs.filebrowser.infra.serviceImpl.FileIOServiceImpl;

import java.io.File;

public class FileFragment extends Fragment {

    private FileViewModel slideshowViewModel;
    private FragmentFileBinding binding;

    private FileIOService fileIOService = new FileIOServiceImpl();
    private String storagePath = "";
    private String fileName = "";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider(this).get(FileViewModel.class);

        binding = FragmentFileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        storagePath = getArguments().getString("storagePath");
        fileName = getArguments().getString("fileName");

        final TextView textView = binding.txtContent;
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                String text = fileIOService.load(storagePath + File.separator + fileName);
                textView.setText(text);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}