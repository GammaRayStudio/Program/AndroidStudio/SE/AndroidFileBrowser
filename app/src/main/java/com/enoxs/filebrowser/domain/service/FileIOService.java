package com.enoxs.filebrowser.domain.service;

import java.util.List;

public interface FileIOService {

    /**
     * 判斷檔案或資料夾是否存在
     */
    boolean isExist(String path);

    /**
     * 判斷外部的儲存裝置，是否掛載 ?
     */
    boolean isMounted(String path);

    /**
     * 載入檔案
     */
    String load(String path);

    /**
     * Android 功能 : 回傳該資料夾路徑下，所有的檔案與資料夾的「名稱」
     *
     * @param path : 目標資料夾的路徑
     * @return :
     *
     * + List.get(0) - 資料夾的陣列名稱
     * + List.get(1) - 檔案的陣列名稱
     *
     * list.size() == 2 - 資料夾存在，但裡面的檔案是空的
     * list.size() == 0 - 資料夾不存在
     */
    List<String[]> listDirInfo(String path);

    /**
     * 創建檔案
     * @param path : 檔案路徑
     * @param name : 檔案名稱
     */
    boolean createFile(String path , String name);

    /**
     * 創建資料夾
     * @param path : 資料夾路徑
     */
    boolean createFolder(String path);

}
