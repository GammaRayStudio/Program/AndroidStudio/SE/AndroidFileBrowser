參考資料
======


Feature
------
### MainActivity 取得 Fragment 的方法
<https://www.py4u.net/discuss/609377>



工具列 `UI`
------
### How to update UI in a BroadcastReceiver
<https://stackoverflow.com/questions/14643385/how-to-update-ui-in-a-broadcastreceiver>


廣播機制
------
### Using the App Toolbar
<https://guides.codepath.com/android/using-the-app-toolbar>

